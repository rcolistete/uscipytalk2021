# uSciPyTalk2021 - Scientific MicroPython on Microcontrollers at SciPy 2021 Conference

This talk aims to update the status of MicroPython, promote it for scientific computing and show module development in
MicroPython.

MicroPython (http://micropython.org/) is a FOSS implementation of Python 3 optimised to run on microcontrollers with few
MHz and 16 Kbytes of RAM. It was created in 2013 by the Physicist Damien P. George.

MicroPython on microcontroller is like a low-level Python operating system running interactive prompt or scripts.
Started with the Pyboard on 2013, MicroPython and its derivatives currently have more than one hundred of commercially
available microcontroller boards, some of them have MicroPython pre-installed, others can have MicroPython installed by the
user. Some MicroPython boards have features like few MHz to almost 1 GHz, 16KB to 32MB of RAM, tens of kB to some
tens of MB of flash memory, WiFi, Bluetooth, LoRa, SigFox, cellular LTE-CATM1/M2, camera, etc. They cost USD 2-100, are
very small and light, about some to tens of mm (milimeters) in each dimension and about 5-10 g, have low power
consumption, so MicroPython boards are affordable and can be embedded in almost anywhere.

MicroPython microcontroller boards have many electronic interfaces : digital input/output (GPIO) ports, analog inputs (via
Analog Digital Converter - ADC), analog outputs (via Digital to Analog Converter - ADC), I2C, SPI, PWM, wireless (WiFi,
Bluetooth, LoRa, SigFox, etc), etc. So MicroPython on microcontroller boards can be used to control all kinds of electronic
and IoT (Internet of Things) projects.

In terms of software, MicroPython is easier and more productive than programming with Arduino IDE, C/C++, etc. Being
Python, it is well suited for Internet programming, so MicroPython boards are a natural choice for IoT (Internet of Things), for example running a simple web server to show a sensor output (text and graphics), sending sensor data to IoT cloud, etc.

This fact is very important as today there are about 50 billions of IoT devices worldwide, so scientific MicroPython can
increase the user base of scientific Python.

MicroPython has already some scientific modules available in some areas : statistics, micro-NumPy/SciPy (ulab, etc),
calculations with uncertainties, vector and matrix calculations, digital filter, neural networks/machine learning tools
(TensorFlow Lite, etc), etc.

Float point support in MicroPython can be single precision (FP32) or double precision (FP64), and some microcontroller
boards have hardware support for FP32 and even FP64.

The talk will also show that there are 5 different methods to create modules in MicroPython, with different optimizations in
memory and performance : plain text .py scripts, .mpy precompiled modules, frozen modules in bytecode, dynamic native
modules (in C, etc) and native modules (in C).

Porting pure Python 3 modules to MicroPython is possible for modules with few dependencies and low RAM usage. The
community effort on MicroPython code developing can be worth because the high optimization level can also be reverted
back to make better pure Python 3 code.

A brief demo will show :
- MicroPython using some scientific modules to process the data of a sensor connected to a microcontroller;
- creating .mpy precompiled module and comparing its usage;
- compiling MicroPython source code with a frozen module in bytecode and an additional native module in C, both for
scientific calculations.

The message is : MicroPython on microcontrollers is viable plataform for scientific computing applied to Physical Computing
(sensors and actuators) and IoT (Internet of Things) with its 50 billions of connected devices.

Related to this talk, SciPy 2021 also has a poster about ulab benchmarks and a sprint about MicroPython that were
submitted by the same author (Roberto Colistete Jr).

This talk is receiving updates (citing new hardware, new MicroPython modules, etc).

Soon : Jupyter Notebook slideshow with RISE.


